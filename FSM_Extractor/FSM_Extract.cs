﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Svg;
using TSOBROWSERLib;
using System.Xml;
using System.IO;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace FSM_Extractor
{
    public partial class FSM_Extract : Form
    {
        private SynchronizationContext synchronizationContext; //https://www.codeproject.com/Articles/37642/Avoiding-InvokeRequired#Official
        private string curr_path = "";
        private int img_num = 0;
        private List<string> evtm_index = new List<String>();
        private List<string> epl = new List<String>(); //This will contain the EPL data for faster loading
        private string modelYear = "";
        private string modelName = "";
        private string output_path;// Output path for complete extraction (after cleanup/modification has been completed);
        private string base_path;//Output path for base extraction ;
        private string baseusein = "";//useni4;treni4   //Which folder under content (Will autoselect the one containing *en* or ask the user to select)
        private string discContent;   //Needs to be the content folder from the service manual disc itself
        private string extractLocation = System.IO.Path.GetTempPath(); //For the EPL files (1kb each, <100 per disc), just needs a temp dir location
        private string loadingPage; //Custom :loading: page to display in the FSM
        private UIDialogInterface TSOInterface;
        private PreferenceState TSOInterface_PS;
        private bool debug = true;

        protected delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
        protected static extern int GetWindowText(IntPtr hWnd, StringBuilder strText, int maxCount);
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
        protected static extern bool EnumWindows(EnumWindowsProc enumProc, IntPtr lParam);
        protected static bool EnumTheWindows(IntPtr hWnd, IntPtr lParam)
        {
            StringBuilder sb = new StringBuilder(18);
            GetWindowText(hWnd, sb, 18);
            if (sb.ToString() == "Missing volume...")
            {
                //log(sb.ToString());
                return false;
            }
            return true;
        }
        public FSM_Extract()
        {
            this.synchronizationContext = SynchronizationContext.Current;
            this.FormClosing += FSM_Extract_FormClosing;
            InitializeComponent();
            loadSettings();
        }

        
        static void Main(string[] args)
        {
            Application.Run(new FSM_Extract());
        }

        private void log(string log_text, bool write_debug = false)
        {
            if (debug == true || write_debug == false)
            {
                synchronizationContext.Send(new SendOrPostCallback(delegate (object state)
                {
                    txtOutput.AppendText(log_text + "\r\n");
                    try { File.AppendAllText("log.txt", log_text + "\n", Encoding.UTF8); } catch { }
                }), null);
            }
        }

        private void updateListing(string Name)
        {
            synchronizationContext.Send(new SendOrPostCallback(delegate (object state)
            {
                lsvTSODisplay.Items.Remove(lsvTSODisplay.FindItemWithText(Name));
                lsvTSODisplay.Items.Add(Name);
            }), null);
        }

        private void setProgressStatus()
        {
            synchronizationContext.Send(new SendOrPostCallback(delegate (object state)
            {
                pgbMain.Increment(1);
            }), null);
        }

        private void setProgressMax(int Max)
        {
            synchronizationContext.Send(new SendOrPostCallback(delegate (object state)
            {
                pgbMain.Value = 0;
                pgbMain.Maximum = Max;
            }), null);
        }

        private void loadSettings()
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            XmlReader reader = XmlReader.Create("settings.xml", settings);
            log("**** Settings ****\n");
            reader.MoveToContent();
            while (reader.Read())
            {
                if (reader.IsStartElement())
                {
                    switch (reader.Name)
                    {
                        case "output_path":

                            if (reader.Read())
                            {
                                if (reader.Value.Trim() != "")
                                {
                                    log("Output Path: " + reader.Value.Trim());
                                    output_path = reader.Value.Trim();
                                }
                            }
                            break;
                        case "base_path":

                            if (reader.Read())
                            {
                                if (reader.Value.Trim() != "")
                                {
                                    log("Base Path: " + reader.Value.Trim());
                                    base_path = reader.Value.Trim();
                                }
                            }
                            break;
                        case "FSM_Load_Page_path":

                            if (reader.Read())
                            {
                                if (reader.Value.Trim() != "")
                                {
                                    log("FSM Load Page Path: " + reader.Value.Trim());
                                    loadingPage = reader.Value.Trim();
                                }
                            }
                            break;
                        case "baseusin":

                            if (reader.Read())
                            {
                                if (reader.Value.Trim() != "")
                                {
                                    log("Baseusin: " + reader.Value.Trim());
                                    baseusein = reader.Value.Trim();
                                }
                            }
                            break;
                        case "disc_path":

                            if (reader.Read())
                            {
                                if (reader.Value.Trim() != "")
                                {
                                    log("Disc Path: " + reader.Value.Trim());
                                    discContent = reader.Value.Trim();
                                }
                            }
                            break;
                        case "temp_path":

                            if (reader.Read())
                            {
                                if (reader.Value.Trim() != "")
                                {
                                    log("Temp Path: " + reader.Value.Trim());
                                    extractLocation = reader.Value.Trim();
                                }
                            }
                            break;
                    }
                }
            }
            
            //Makes sure paths exists or creates them
            try
            {

                if (!Directory.Exists(output_path))
                {
                    Directory.CreateDirectory(output_path);
                }
                if (!Directory.Exists(base_path))
                {
                    Directory.CreateDirectory(base_path);
                }
                if (!Directory.Exists(extractLocation))
                {
                    Directory.CreateDirectory(extractLocation);
                }
            }
            catch
            {
                log("\n**** Could not create Dirs! Please check directory settings! ****\n");
            }
            
            if (Directory.Exists(extractLocation) && Directory.GetFiles(extractLocation, "*.epl").Count() >= 1)
            {
                log("Found existing EPL's. If you do not wish to use these, please delete " + extractLocation);
                //getEPL(extractLocation);
            }

            log("\n**** Main App ****\n");

            createTSOInterface();

            //Create the preference interface to be able to get the content directory as well as the application directory
            //Might be able to setup the USEIN directory based of the wulist.txt file in the appdir directory
            TSOInterface_PS = new TSOBROWSERLib.PreferenceState();
            //log(TSOInterface_PS.GetPreference("appdir"));
            string content_path = TSOInterface_PS.GetPreference("contentdir");
            if (Directory.Exists(content_path))
            {
                discContent = content_path;
                log("Using disc content directory from TSO Browser: " + content_path);
            }
        }

        private void createTSOInterface()
        {
            TSOInterface = new TSOBROWSERLib.UIDialogInterface();
            System.Diagnostics.Process[] tsoBrowser = System.Diagnostics.Process.GetProcessesByName("tsobrowser");
            if (tsoBrowser.Count() <= 0) { tsoBrowser = System.Diagnostics.Process.GetProcessesByName("tsobro~1"); }
            if (tsoBrowser[0].MainWindowTitle.Contains("Service Information"))
            {
                if (!EnumWindows(new EnumWindowsProc(EnumTheWindows), IntPtr.Zero))
                {
                    log("Please fix FSM Application. It appears that it cannot find the disc/files!");
                }
            }
            else
            {
                log("Please fix FSM Application. It does not appear to be opening...");
            }
            
            TSOInterface.Navigate(loadingPage, loadingPage);

            
        }

        private void closeTSOInterface()
        {
            TSOInterface.ExitApplication();
        }

        private void FSM_Extract_Load(object sender, EventArgs e)
        {
        }

        private void FSM_Extract_FormClosing(Object sender, FormClosingEventArgs e)
        {
            try
            {
                // TSOInterface.ExitApplication();
                closeTSOInterface();
            }
            catch
            {

            }
        }

        private string getTSOType(string fileLoc)
        {
            //We need to parse the file and remove '&'
            string text = File.ReadAllText(fileLoc);
            text = text.Replace("&", "AMPERSANDERS");
            File.WriteAllText(fileLoc, text);

            XmlDocument doc = new XmlDocument();
            doc.Load(fileLoc);
            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/workunit");
            string type = "";
            foreach (XmlNode node in nodes)
            {
                type = node.SelectSingleNode("type").InnerText;
            }

            //We need to parse the file and replace '&'
            text = File.ReadAllText(fileLoc);
            text = text.Replace("AMPERSANDERS", "&");
            File.WriteAllText(fileLoc, text);

            //EVTM --> Wiring diagrams/RECALL --> Recall/SERVICE --> Service Manual/TSB --> TSB/PCED --> Engine diag
            switch (type)
            {
                case "EVTM":
                    return "Wiring_Diagrams";
                case "RECALL":
                    return "Recall";
                case "SERVICE":
                    return "Service_Manual";
                case "TSB":
                    return type;
                case "PCED":
                    return "Powertrain_Emissions_Diagnosis";
                default:
                    return type;
            }
        }

        private void refineTSO(string tsoloc)
        {
            //log("Refining TSO");
            string TSODir = tsoloc;
            log("Refining TSO: " + TSODir,true);
            if (TSODir.Length <= 0)//TODO: Make this an input box
            {
                Console.Write("Please Enter Directory:");
                TSODir = Console.ReadLine();
            }
            string section = "";
            string[] files = System.IO.Directory.GetFiles(TSODir, "*.epl");
            //section = files[0].Split('\\')[files[0].Split('\\').Count() - 1].Substring(0, 3);
            section = files[0].Split('\\')[files[0].Split('\\').Count() - 1];
            section = Regex.Match(section, "([0-9a-zA-Z]*)").Value;
            //([0-9a-zA-Z]*)
            //Type
            //EVTM --> Wiring diagrams/RECALL --> Recall/SERVICE --> Service Manual/TSB --> TSB/PCED --> Engine diag
            string type = getTSOType(files[0]);
            string resourcesPath = "Resources\\" + type + "\\";//"\\Resources\\" + type + "\\";

            // string remove_part_one = section +  "~us~en~file="; //ex. eer~us~en~file=

            //Have an array of what the ends can be. Ex. svg, pdf, gif,png,jpg,htm,xml,csv
            string[] remove_part_two_array = { "~gen~ref.svg", "~gen~ref.pdf", "~gen~ref.gif", "~gen~ref.pdf", "~gen~ref.png", "~gen~ref.jpg", "~gen~ref.htm", "~gen~ref.xml", "~gen~ref.csv", (section + "~us~en~file="), (section + "~tr~en~file=") };
            //string remove_part_two = "~gen~ref."; //ex. ~gen~ref.svg

            string[] dirs = Directory.EnumerateFiles(TSODir).ToArray();
            string TSOResource = Path.Combine(TSODir, resourcesPath);
            Directory.CreateDirectory(TSOResource);
            setProgressMax(dirs.Count());
            foreach (string file in dirs)
            {
                log("File: " + file,true);
                setProgressStatus();
                FileInfo current_file = new FileInfo(file);
                // string stripped_name = (current_file.Name.ToLower()).Replace(remove_part_one,"");
                string stripped_name = removeEnds(remove_part_two_array, (current_file.Name.ToLower()));
                // log(stripped_name);                
                current_file.MoveTo(Path.Combine(current_file.DirectoryName , resourcesPath , stripped_name));
                if (current_file.Extension.ToLower() == ".htm")
                {
                    refineHTM(remove_part_two_array, (current_file.DirectoryName + "\\" + stripped_name));
                    if (stripped_name.ToLower() == section + "style.htm")
                    {
                        current_file.MoveTo(Path.Combine(current_file.DirectoryName, (stripped_name.Replace(".htm",".css"))));
                    }
                    else
                    {      
                        //if (type == "Powertrain_Emissions_Diagnosis") //We seem to have these links through the service manual, no need to only work PCED
                        //{
                        // log("................................Working...................");
                        string text = File.ReadAllText((current_file.DirectoryName + "\\" + stripped_name));
                        string to_replace = section + "~us~en~leftside=";
                        text = Regex.Replace(text, to_replace, "", RegexOptions.IgnoreCase);
                        text = Regex.Replace(text, "_parent", "leftside", RegexOptions.IgnoreCase);
                        to_replace = "&rightside=([0-9a-zA-Z]*).HTM&market=us&lang=en~gen~2col.htm";
                        text = Regex.Replace(text, to_replace, "", RegexOptions.IgnoreCase);
                        to_replace = section + "STYLE.HTM";
                        text = Regex.Replace(text, to_replace, (section + "STYLE.css"), RegexOptions.IgnoreCase);
                        File.WriteAllText((current_file.DirectoryName + "\\" + stripped_name), text);
                    }
                    
                    //}
                }
                /*else if(current_file.Extension.ToLower() == ".svg")
                {
                    Directory.CreateDirectory(TSODir + resourcesPath + "Resources\\Wiring_Diagrams");
                    string svg_path = current_file.DirectoryName  + "\\" + stripped_name;
                    svg2png(svg_path);
                }*/


            }
            //Find section + main.htm file, and modify it so it can be all ALONE
            //We need to modify section + left.htm and section + right.htm to be SUBFOLDER/section + SIDE.htm
            //For PC/ED we need to replace ve2~us~en~leftside=VE2LEFT.HTM&rightside=VE2RIGHT.HTM&market=us&lang=en~gen~2col.htm

            if (type != "Wiring_Diagrams" && type != "TSB")
            {
                string text = File.ReadAllText(TSOResource + section + "main.htm");
                //text = text.ToLower().Replace(section + "left.htm", "Resources\\" + type + "\\" + section + "left.htm");
                text = Regex.Replace(text, section + "left.htm", "Resources\\" + type + "\\" + section + "left.htm", RegexOptions.IgnoreCase);
                //text = text.ToLower().Replace(section + "right.htm", "Resources\\" + type + "\\" + section + "right.htm");
                text = Regex.Replace(text, section + "right.htm", "Resources\\" + type + "\\" + section + "right.htm", RegexOptions.IgnoreCase);
                File.WriteAllText(TSOResource + section + "main.htm", text);
                FileInfo mainfile = new FileInfo(TSOResource + section + "main.htm");
                mainfile.CopyTo(TSODir + "\\" + (type.Replace("_", " ")) + ".htm");

                //Copy to main year/model folder
                string dest_path = base_path + "\\" + (modelYear + " " + modelName) + "\\Resources\\" + type + "\\";
                if (!System.IO.Directory.Exists(dest_path))
                {
                    System.IO.Directory.CreateDirectory(dest_path);
                }
                File.Copy(TSODir + (type.Replace("_", " ")) + ".htm", base_path + "\\" + (modelYear + " " + modelName) + "\\" + (type.Replace("_", " ")) + ".htm");
                //string dest_path = base_path + "\\" + (modelYear + " " + modelName) + "\\Resources\\" + type + "\\";
                //if (!System.IO.Directory.Exists(dest_path))
                //{
                //      System.IO.Directory.CreateDirectory(dest_path);
                // }
                log("Copying TSO Files");
                var Files = Directory.GetFiles(TSOResource);
                setProgressMax(Files.Count());
                foreach (string file in Files)
                {
                    setProgressStatus();
                    FileInfo file2copy = new FileInfo(file);
                    file2copy.CopyTo(dest_path + file2copy.Name, true);
                }
                fixCapsIssue(dest_path);
            }
            if (type == "Powertrain_Emissions_Diagnosis")
            {
                // log("................................Working...................");
                string text = File.ReadAllText(TSOResource + section + "left.htm");
                string to_replace = section + "~us~en~leftside=";
                text = Regex.Replace(text, to_replace, "", RegexOptions.IgnoreCase);
                text = Regex.Replace(text, "_parent", "leftside", RegexOptions.IgnoreCase);
                to_replace = "&rightside=([0-9a-zA-Z]*).HTM&market=us&lang=en~gen~2col.htm";
                text = Regex.Replace(text, to_replace, "", RegexOptions.IgnoreCase);
                File.WriteAllText(TSOResource + section + "left.htm", text);
            }
            if (type == "TSB")
            {
                string text = File.ReadAllText(TSOResource + section + ".htm");
                //text = text.ToLower().Replace(section + "left.htm", "Resources\\" + type + "\\" + section + "left.htm");
                text = Regex.Replace(text, section + "left.htm", "Resources\\" + type + "\\" + section + "left.htm", RegexOptions.IgnoreCase);
                //text = text.ToLower().Replace(section + "right.htm", "Resources\\" + type + "\\" + section + "right.htm");
                text = Regex.Replace(text, section + "right.htm", "Resources\\" + type + "\\" + section + "right.htm", RegexOptions.IgnoreCase);
                File.WriteAllText(TSOResource + section + ".htm", text);
            }
            if (type == "Wiring_Diagrams")
            {

                string[] file_list = Directory.EnumerateFiles(TSOResource, "*.svg").ToArray();
                log("Converting SVGs to PNG");
                foreach (string svg_file in file_list)
                {
                    svg2png(svg_file);
                }
                //createEVTMIndex(TSOResource + resourcesPath, (modelYear + " " + modelName));
                createEVTMIndex(TSOResource, (modelYear + " " + modelName));
                string dest_path = Path.Combine(base_path , (modelYear + " " + modelName) , "Resources\\Wiring_Diagrams\\");
                if (!System.IO.Directory.Exists(dest_path))
                {
                    System.IO.Directory.CreateDirectory(dest_path);
                }
                File.Copy(Path.Combine(TSOResource, "Wiring Diagrams.htm"), Path.Combine(base_path, (modelYear + " " + modelName), "Wiring Diagrams.htm"));//(base_path + "\\" + (modelYear + " " + modelName) + "\\Wiring Diagrams.htm")
                log("Copying TSO Files");
                var Files = Directory.GetFiles(TSOResource);
                setProgressMax(Files.Count());
                foreach (string file in Files)
                {
                    setProgressStatus();
                    FileInfo file2copy = new FileInfo(file);
                    file2copy.CopyTo(Path.Combine(dest_path, file2copy.Name), true);
                }
                fixCapsIssue(dest_path);
            }
        }

        private void fixCapsIssue(string path)
        {
            //This function will enumerate all files
            //It will then search and replace ALL instances in HTM/HTML files with proper links
            //This is to fix linking issues on non-Windows systems where caps matter in the path
            log("Obtaining File List for Caps Issue");

            string[] Files = Directory.GetFiles(path);
            setProgressMax(Files.Count());
            List<string> fileNameList = new List<string>();
            foreach (string file in Files)
            {
                setProgressStatus();
                string fileName = new FileInfo(file).Name;
                fileNameList.Add(fileName);
            }

            Files = Directory.GetFiles(path,"*.htm");
            setProgressMax(Files.Count());
            foreach (string file in Files)
            {
                setProgressStatus();
                string fileText = File.ReadAllText(file);
                foreach (string filename in fileNameList)
                {
                    //Use regex to ignore-case when finding match
                    //And replace it with the case we actually have
                    
                    fileText = Regex.Replace(
                    fileText,
                    Regex.Escape(filename),
                    filename.Replace("$", "$$"),
                    RegexOptions.IgnoreCase
                );
                    
                }
                File.WriteAllText(file, fileText);
            }

        }

        private void openEVTM_MDB_File(string mdb_file)
        {
            //TODO: Determine actual structure of file and how to recreate the wiring diagram
            //web pages from the database

            // Connection string to mdb file 
            string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + mdb_file;
            string strSQL = "SELECT * FROM CELLS";
            //Table names:
            /* 
             * 
             * CELLS
             * CONN
             * CONNREF
             * GRND
             * GRNDREF
             * LOCREF
             * SPLCREF
             * SPLICE
             * comp
             * compref
             * pageref
             * 
             * Use: https://www.mdbopener.com/ as a reference
             */
            // Create a connection to the mdb file
            using (System.Data.OleDb.OleDbConnection connection = new System.Data.OleDb.OleDbConnection(connectionString))
            {
                System.Data.OleDb.OleDbCommand command = new System.Data.OleDb.OleDbCommand(strSQL, connection);
                try
                {
                    connection.Open();
                    using (System.Data.OleDb.OleDbDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Console.WriteLine("{0} {1}", reader["CELLTYPE"].ToString(), reader["TITLE"].ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void refineHTM(string[] remove_array, string file_name)
        {
            log("Refining: " + file_name, true);
            string text = File.ReadAllText(file_name);
            foreach (string remove_string in remove_array)
            {
                //text = text.ToLower().Replace(remove_string, "");
                text = Regex.Replace(text, remove_string, "", RegexOptions.IgnoreCase);
            }
            File.WriteAllText(file_name, text);
        }
        private string removeEnds(string[] remove_array, string input_string)
        {
            string output_string = input_string;
            foreach (string remove_string in remove_array)
            {
                output_string = Regex.Replace(output_string, remove_string, "", RegexOptions.IgnoreCase);
            }
            return output_string;
        }

        private string getBaseusein()
        {
            if (baseusein == "")
            {
                string[] dirs = Directory.GetDirectories(discContent, "*en*"); //Try catch needed
                if (dirs.Count() == 1)
                {
                    string dir = dirs[0].Split('\\')[dirs[0].Split('\\').Count() - 1];
                    log("Selected content directory: " + dir);
                    return dir;
                }
                else
                {
                    log("Please enter content directory: "); //TODO: Remove Console.Readline
                    foreach (string dir in dirs)
                    {
                        log(dir.Split('\\')[dir.Split('\\').Count() - 1]);
                    }
                    return Console.ReadLine();
                }
            }
            else
            {
                log("Selected content directory: " + baseusein);
                return baseusein;
            }

        }

        private void getEPL(string path)
        {
            epl.Clear();
            string[] fileEntries = Directory.GetFiles(path,"*.epl");
            log("Getting EPL files...");
            setProgressMax(fileEntries.Count());
            foreach (string fileName in fileEntries)
            {
                log("   " + fileName, true);
                setProgressStatus();
                //string TSOType = getTSOType(fileName);
                string type = "";
                string sep = "~";
                string arcName = "";
                string model = "";
                string year = "";

                try
                {
                    //We need to parse the file and remove '&'
                    string text = File.ReadAllText(fileName);
                    text = text.Replace("&", "AMPERSANDERS");

                    //Parse the descriptor, create dir, set vars and continue to next loop
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(text);
                    XmlNodeList nodes = doc.DocumentElement.SelectNodes("/workunit");

                    foreach (XmlNode node in nodes)
                    {
                        arcName = node.SelectSingleNode("code").InnerText;
                        type = node.SelectSingleNode("type").InnerText;
                        if (type == "EVTM" || type == "SERVICE")
                        {
                           // year = node.SelectSingleNode("vehicles/vehicle/year").InnerText;
                           // model = node.SelectSingleNode("vehicles/vehicle/name").InnerText;
                            
                            foreach(XmlNode subnode in node.SelectNodes("vehicles/vehicle"))
                            {
                                //log(subnode.SelectSingleNode("year").InnerText + " " + subnode.SelectSingleNode("name").InnerText);
                                year = subnode.SelectSingleNode("year").InnerText;
                                model = subnode.SelectSingleNode("name").InnerText;

                                epl.Add(arcName + sep + year + sep + model + sep + type);
                            }
                            
                        }
                        else if (type == "PCED" || type == "TSB" || type == "RECALL")
                        {
                            model = GetSafeFilename(node.SelectSingleNode("title").InnerText);
                            epl.Add(arcName + sep + year + sep + model + sep + type);
                        }
                        //arcName = node.SelectSingleNode("code").InnerText;
                        
                    }

                }
                catch
                {
                    log("Could not get: " + fileName);
                }
                //epl.Add(arcName + sep + year + sep + model + sep + type);
                log(arcName + sep + year + sep + model + sep + type,true);
            }
        }

      
        private void getArcFileContents(string fileName, string year_input = "", string model_input = "", string type_input = "LIST")
        {
            log("Getting ArcFile Contents for: FileName[" + fileName + "] Year["+ year_input + "] Model[" + model_input + "] Type[" + type_input + "]", true);

            using (TextReader tr = new StreamReader(new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                string line;
             
                while (((line = tr.ReadLine()) != null) )
                {
                    if ((Regex.Matches(line.ToLower(), "jpg").Count >= 5 || Regex.Matches(line.ToLower(), "gif").Count >= 5 || Regex.Matches(line.ToLower(), "htm").Count >= 5 || Regex.Matches(line.ToLower(), "xml").Count >= 5 || Regex.Matches(line.ToLower(), "pdf").Count >= 5 || Regex.Matches(line.ToLower(), "svg").Count >= 5) && (line.Contains("IDICOMP") || line.Contains("idicomp")))
                    {
                        //Look for: GIF,JPG,PDF,HTM,XML,SVG -- Skip GIF and PDF since there seems to be too few to count on...
                        string[] idi = new string[] { "IDICOMP" };
                        string final_result = line.Split(idi, StringSplitOptions.RemoveEmptyEntries)[0];

                        final_result = final_result.Remove(final_result.Length - 1); //End of string should be "proper" with no boxed ?

                        string[] final_result_two = final_result.Split((char)0); //Removes all the crap from the front of the string (char)0 being a blank something
                        final_result = final_result_two[final_result_two.Count() - 1];

                        if (final_result.Contains(".epl") || final_result.Contains(".EPL")) //ex. ser.epl
                        {
                            log("Contains EPL Descriptor!");
                            if (getFile(fileName, final_result, year_input, model_input, type_input))
                            {
                                log("Finished extracting!");
                                break;
                            }
                            break;
                        }
                        else
                        {
                            log(fileName + " Does NOT Contain EPL Descriptor!"); //TODO: Add to debug
                        }
                    }
                }
            }
        }

        private void getArcFileContents_old(string fileName, string year_input, string model_input, string type_input)
        {
            log("Getting ArcFile Contents for: FileName[" + fileName + "] Year[" + year_input + "] Model[" + model_input + "] Type[" + type_input, true);
            string[] arc_file_contents = File.ReadAllLines(fileName);
            log("File contains [" + arc_file_contents.Length + "] lines");
            setProgressMax(arc_file_contents.Count());
            foreach (string line in arc_file_contents)
            {
                setProgressStatus();
                //if(line.ToUpper().Contains("JPG")  || line.ToUpper().Contains("HTM") || line.ToUpper().Contains("XML") || line.ToUpper().Contains("SVG"))
                if ((Regex.Matches(line.ToLower(), "jpg").Count >= 5 || Regex.Matches(line.ToLower(), "gif").Count >= 5 || Regex.Matches(line.ToLower(), "htm").Count >= 5 || Regex.Matches(line.ToLower(), "xml").Count >= 5 || Regex.Matches(line.ToLower(), "pdf").Count >= 5 || Regex.Matches(line.ToLower(), "svg").Count >= 5) && (line.Contains("IDICOMP") || line.Contains("idicomp")))
                {
                    //Look for: GIF,JPG,PDF,HTM,XML,SVG -- Skip GIF and PDF since there seems to be too few to count on...
                    string[] idi = new string[] { "IDICOMP" };
                    string final_result = line.Split(idi, StringSplitOptions.RemoveEmptyEntries)[0];
                    final_result = final_result.Remove(final_result.Length - 1); //End of string should be "proper" with no boxed ?
                                                                                 //string[] final_result_two = final_result.Split(null as string[], StringSplitOptions.RemoveEmptyEntries);
                    string[] final_result_two = final_result.Split((char)0); //Removes all the crap from the front of the string (char)0 being a blank something
                    final_result = final_result_two[final_result_two.Count() - 1];
                    //log(final_result);
                    // FileInfo File = new FileInfo(fileName);
                    if (final_result.Contains(".epl") || final_result.Contains(".EPL")) //ex. ser.epl
                    {
                        log("Contains EPL Descriptor!");
                        if (getFile(fileName, final_result, year_input, model_input, type_input))
                        {
                            log("Finished extracting!");
                            break;
                        }
                    }
                    else
                    {
                        log(fileName + " Does NOT Contain EPL Descriptor!"); //TODO: Add to debug
                    }
                }
            }
        }

        private void getTSO(string year_input, string model_input, string type_input)
        {
            baseusein = getBaseusein();

            string idType = "";
            switch (type_input.Split(':')[0])
            {
                case "SERVICE":
                    idType = "S";
                    break;
                case "EVTM":
                    idType = "E";
                    break;
                case "TSB":
                    idType = "T";
                    break;
                case "RECALL":
                    idType = "R";
                    break;
                case "PCED":
                    idType = "V";
                    break;
                case "ALL":
                    idType = "*";
                    break;
                default:
                    idType = type_input.Split(':')[1];
                    break;
            }

            if (Directory.Exists(extractLocation) && Directory.GetFiles(extractLocation, "*.epl").Count() >= 1)
            {
                getEPL(extractLocation);
                string type = "";
                char sep = '~';
                string arcName = "";
                string model = "";
                string year = "";
                bool found = false;
                foreach (string epl_file_desc in epl)
                {
                    arcName = epl_file_desc.Split(sep)[0];
                    year = epl_file_desc.Split(sep)[1];
                    model = epl_file_desc.Split(sep)[2];
                    type = epl_file_desc.Split(sep)[3];
                    Boolean extract = sbExtractd(year, model, type, year_input, model_input, type_input.Split(':')[0]);
                    log("ArcName: " + arcName + " Year: " + year + " Model: " + model + " Type: " + type + " Extract: " + extract, true);
                    if (extract && (type_input.Split(':').Count() <= 1)) //Refactor with extract function
                    {
                        //Lets check for arcname.txt
                        //arcname.txt will contain ALL files from the Arc
                        //This should improve overall process time

                        found = true;
                        string arcPath = Path.Combine(discContent, baseusein, arcName + ".arc");
                        if (File.Exists(Path.Combine(extractLocation, (arcName + ".txt"))))
                        {
                            getFile(arcPath, "", year, model, type);
                        }
                        else
                        {  
                            log("Arc Path: " + arcPath, true);
                            //log("Arc Path: " + arcPath);
                            getArcFileContents(arcPath, year, model, type);                            
                        }
                        break;


                    }
                    else if (extract && (type_input.Split(':')[1] == "LIST"))
                    {
                        found = true;
                        updateListing(year + sep + model + sep + type);
                    }
                }
                if(!found)
                {
                    if (type_input.Split(':')[1] == "LIST")
                    {
                        log("No " + type_input.Split(':')[0] + " manuals available.");
                    }
                    else
                    {
                        log("Could not find: " + year_input + " " + model_input + " " + type_input);
                    }
                }


            }
            else
            {
                string[] fileEntries = Directory.GetFiles(Path.Combine(discContent, baseusein) + "\\", idType + "*");                
                setProgressMax(fileEntries.Count());
                log("Getting base files...");
                foreach (string fileName in fileEntries)
                {
                    log("File Entry: " + fileName, true);
                    String shortFileName = fileName.Split('\\')[fileName.Split('\\').Count() - 1];
                    setProgressStatus();

                    if (shortFileName[0].ToString().ToUpper().Equals(idType) || idType == "*")
                    {
                        //log("Obtaining...");
                        log("Obtaining: " + shortFileName, true);
                        getArcFileContents(fileName);
                    }
                }
                log("Finished", true);
            }
        }

        private Boolean getFile(string arc_name, string file_name, string year_input, string model_input, string type_input)
        {
            string arc_file = arc_name.ToLower().Replace(".arc", "");
            string shortARC = arc_file.Split('\\')[arc_file.Split('\\').Count() - 1];
            
            modelYear = "";
            modelName = "";
            string type = ""; //EVTM --> Wiring diagrams/RECALL --> Recall/SERVICE --> Service Manual/TSB --> TSB/PCED --> Engine diag
            string extract_dir_name = "";

            List<string> Files;
            List<string> models = new List<string>();

            //Check to see if we already have the EPL for the ARC
            if (File.Exists(Path.Combine(extractLocation, (shortARC + ".epl"))) && File.Exists(Path.Combine(extractLocation, (shortARC + ".txt"))))
            {
                //We need to parse the file and remove '&'
                string text = File.ReadAllText(Path.Combine(extractLocation, (shortARC + ".epl")));
                text = text.Replace("&", "AMPERSANDERS");

                //Parse the descriptor, create dir, set vars and continue to next loop
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(text);
                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/workunit");

                foreach (XmlNode node in nodes)
                {
                    type = node.SelectSingleNode("type").InnerText.ToUpper();
                    if (type == "EVTM" || type == "SERVICE")
                    {
                        // modelYear = node.SelectSingleNode("vehicles/vehicle/year").InnerText;
                        //modelName = node.SelectSingleNode("vehicles/vehicle/name").InnerText;
                        foreach (XmlNode subnode in node.SelectNodes("vehicles/vehicle"))
                        {
                            //log(subnode.SelectSingleNode("year").InnerText + " " + subnode.SelectSingleNode("name").InnerText);
                            modelYear = subnode.SelectSingleNode("year").InnerText;
                            modelName = subnode.SelectSingleNode("name").InnerText;
                            models.Add(modelYear + "~" + modelName);
                        }
                    }
                    else if (type == "PCED" || type == "TSB" || type == "RECALL")
                    {
                        modelName = GetSafeFilename(node.SelectSingleNode("title").InnerText);
                        models.Add(modelYear + "~" + modelName); //TODO: This probably needs changed
                    }

                }

                //extract_dir_name = modelYear + "_" + modelName + "_" + type + "\\";
                if (type_input.ToUpper().Contains("LIST"))
                {
                    if (type_input.Split(':')[0] == type || type_input.Split(':')[0] == "ALL" || type_input.Split(':')[0] == "LIST")
                    {
                        log(modelYear + " " + modelName + " " + type);
                        updateListing(modelYear + "~" + modelName + "~" + type);
                    }
                }

                //File listing for the ARC file
                Files = File.ReadAllLines(Path.Combine(extractLocation, (shortARC + ".txt"))).ToList();
            }
            else
            {

                if (file_name != "")
                {
                    Files = new List<String>();
                    Files.Add("");
                    //setProgressMax(file_name.Length);
                    for (int pos = 0; pos <= file_name.Length; pos++)
                    {
                        //setProgressStatus();  //This causes a HUGE overhead. Removing it.
                        try
                        {
                            char isPeriod = file_name[pos];
                            if (isPeriod.Equals('.'))
                            {
                                //Console.Write(string.Concat(isPeriod, result[pos + 1], result[pos + 2], result[pos + 3]));
                                Files[Files.Count - 1] += string.Concat(isPeriod, file_name[pos + 1], file_name[pos + 2], file_name[pos + 3]);
                                Files.Add("");
                                pos += 3;
                            }
                            else
                            {
                                Files[Files.Count - 1] += isPeriod;
                            }
                        }
                        catch { }
                    }
                    //Console.Write(Files[1]);

                    //Lets save the files to a list to map instead of having to re-read the arc files every.single.time.
                    File.WriteAllLines(Path.Combine(extractLocation, (shortARC + ".txt")), Files);
                }
                else
                {
                    Files = File.ReadAllLines(Path.Combine(extractLocation, (shortARC + ".txt"))).ToList();
                }

                foreach (string FileName in Files)
                {
                    //Lets go ahead and get the descriptor, then we can move on
                    if (FileName.ToUpper().Contains(".EPL"))
                    {
                        try
                        {
                            string ExtractedFileName = TSOInterface.Load("tpsreposit/" + baseusein + "//" + FileName); //TODO: Have it grab the local copy instead of extracting it again
                            string trimmedExtractedFileName = (ExtractedFileName.Split('~')[0] + ".epl").Split('\\')[(ExtractedFileName.Split('~')[0] + ".epl").Split('\\').Count() - 1];
                            string copiedExtractedFilePath = Path.Combine(extractLocation, trimmedExtractedFileName);

                            File.Copy(ExtractedFileName, copiedExtractedFilePath, true);

                            //We need to parse the file and remove '&'
                            string text = File.ReadAllText(copiedExtractedFilePath);
                            text = text.Replace("&", "AMPERSANDERS");

                            //Parse the descriptor, create dir, set vars and continue to next loop
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(text);
                            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/workunit");

                            foreach (XmlNode node in nodes)
                            {
                                type = node.SelectSingleNode("type").InnerText;
                                if (type == "EVTM" || type == "SERVICE")
                                {
                                    // modelYear = node.SelectSingleNode("vehicles/vehicle/year").InnerText;
                                    //modelName = node.SelectSingleNode("vehicles/vehicle/name").InnerText;
                                    foreach (XmlNode subnode in node.SelectNodes("vehicles/vehicle"))
                                    {
                                        //log(subnode.SelectSingleNode("year").InnerText + " " + subnode.SelectSingleNode("name").InnerText);
                                        modelYear = subnode.SelectSingleNode("year").InnerText;
                                        modelName = subnode.SelectSingleNode("name").InnerText;
                                        models.Add(modelYear + "~" + modelName);
                                    }
                                }
                                else if (type == "PCED" || type == "TSB" || type == "RECALL")
                                {
                                    modelName = GetSafeFilename(node.SelectSingleNode("title").InnerText);
                                    models.Add(modelYear + "~" + modelName); //TODO: This probably needs changed
                                }

                            }

                            //extract_dir_name = modelYear + "_" + modelName + "_" + type + "\\";
                            if (type_input.Contains("LIST"))
                            {
                                if (type_input.Split(':')[0] == type || type_input.Split(':')[0] == "ALL" || type_input.Split(':')[0] == "LIST")
                                {
                                    log(modelYear + " " + modelName + " " + type);
                                    updateListing(modelYear + "~" + modelName + "~" + type);
                                }
                            }

                        }
                        catch
                        {
                            log("Could not get: " + FileName);
                            //log(errr.ToString());
                        }
                        break;
                    }
                }
            }

            Boolean extract = false;
            foreach (string modelyearname in models)
            {
                modelYear = modelyearname.Split('~')[0];
                modelName = modelyearname.Split('~')[1];
                extract = sbExtractd(modelYear, modelName, type, year_input, model_input, type_input);
                if(extract)
                {
                    break; //This will break out of the loop when it finds the match we are looking for
                }
            }            
           
            if (extract)
            {
                log("sbExtractd data: " + modelYear + " " + modelName + " " + type + " " + year_input + " " + model_input + " " + type_input);
                extract_dir_name = modelYear + "_" + modelName + "_" + type + "\\";
                int fileCount = 0;
                int maxFiles = 1000;

                Directory.CreateDirectory(output_path + extract_dir_name);
                string extractPath = Path.Combine(output_path, extract_dir_name);
                setProgressMax(Files.Count());
                log("Extracting [" + Files.Count() +"] Files");
                foreach (string FileName in Files)
                {         
                    if(fileCount >= maxFiles)
                    {
                        fileCount = 0;
                        log("Hit max file count, restarting interface!");
                        closeTSOInterface();
                        createTSOInterface();
                    }
                    if (!(FileName.Length <= 0))
                    {
                        log("Extracting: " + FileName, true);
                        fileCount++;                        
                        setProgressStatus();
                        try
                        {
                            string TSOFileName = "tpsreposit/" + baseusein + "//" + FileName;
                            //Get the "generated" path for better, more reliable extraction...
                            String filename_ext = FileName.Split('.')[FileName.Split('.').Count() - 1];
                            if (baseusein == "useni4")
                            {
                                TSOFileName = "tpsreposit/" + baseusein + "//" + arc_file + "~us~en~file=" + FileName + "~gen~ref." + filename_ext;
                            }
                            else
                            {
                                TSOFileName = "tpsreposit/" + baseusein + "//" + arc_file + "~tr~en~file=" + FileName + "~gen~ref." + filename_ext;
                            }

                            string ExtractedFileName = TSOInterface.Load(TSOFileName);

                            FileInfo ExtractedFile = new FileInfo(ExtractedFileName);
                            extractPath = Path.Combine(output_path, extract_dir_name, ExtractedFile.Name);
                            ExtractedFile.CopyTo(extractPath);

                        }
                        catch (Exception errr)
                        {
                            log("Could not get: " + FileName);
                            log("Error: " + errr.ToString());
                        }
                    }
                }
                closeTSOInterface();
                //We need "refine" the manual
                refineTSO(output_path + extract_dir_name);

                log("Extracted: " + extract_dir_name);
                return true;
            }
            else
            {
                return false;
            }

        }

        private Boolean sbExtractd(string modelYear, string modelName, string type, string year_input, string model_input, string type_input)
        {
            // Boolean valReturn = false;
            int argument_count = 0;
            int argument_match = 0;
            if (year_input.Length == 4)
            {
                // valReturn = true;
                argument_count++;
                if (modelYear == year_input) { argument_match++; }
            }
            if (model_input.Length >= 1)
            {
                // valReturn = true;
                argument_count++;
                if (modelName == model_input) { argument_match++; }
            }
            if (type_input.Length >= 1)
            {
                // valReturn = true;
                argument_count++;
                if ((type == type_input) || (type_input == "ALL")) { argument_match++; }
            }

            //return valReturn;
            if (argument_count == argument_match)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private string GetSafeFilename(string filename)
        {
            return string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));
        }

        private void svg2png_draw(string output_png, string input_SVG, string svg_name, int size)
        {
            //We need to check the SVG for any images, and add the full path to it so that it can resolve the image
            FileInfo svgFileInfo = new FileInfo(input_SVG);
            string svgFileData = File.ReadAllText(input_SVG);
            string replacement = "xlink:href=\"" + svgFileInfo.DirectoryName + "\\";
            svgFileData = Regex.Replace(
                    svgFileData,
                    Regex.Escape("xlink:href=\""),
                    replacement.Replace("$", "$$"),
                    RegexOptions.IgnoreCase);
            File.WriteAllText(input_SVG, svgFileData);

            if (size != 100)
            {
                //Parse the descriptor, create dir, set vars and continue to next loop
                XmlDocument doc = new XmlDocument();
                doc.Load(input_SVG);
                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/svg");
                foreach (XmlNode node in nodes)
                {
                    node.Attributes["height"].Value = size + "%";
                    node.Attributes["width"].Value = size + "%";
                }
                doc.Save(input_SVG);
            }
            SvgDocument svgDoc = SvgDocument.Open(input_SVG);
            try
            {
                svgDoc.Draw().Save(output_png, ImageFormat.Png);
                if (size != 100) { log("Converted at " + size + "%"); }
            }
            catch (Exception errr)
            {
                if (errr.Message.Contains("Out of memory.") && (size >= 50))
                {
                    size = size - 1;
                    svg2png_draw(output_png, input_SVG, svg_name, size);
                }
                else
                {
                    log(errr.ToString());
                }
                if (size <= 49)
                {
                    log("Could not convert: " + svg_name);
                }
            }
        }
        private void svg2png(string SVG_Path)
        {
            try
            {
                FileInfo SVG_File = new FileInfo(SVG_Path);

                //We need to parse the file and remove rotate(-)
                string text = File.ReadAllText(SVG_Path);
                text = text.Replace("rotate(-)", "");
                File.WriteAllText(SVG_Path, text);

                string input_SVG = SVG_File.FullName;
                string output_SVG = SVG_File.DirectoryName + "\\Resources\\Wiring_Diagrams\\" + SVG_File.Name.Replace(".svg", ".png");
                Directory.CreateDirectory(SVG_File.DirectoryName + "\\Resources\\Wiring_Diagrams\\");
                // string SVG_Content = File.ReadAllText(SVG_Path);
                ////////////////////////////////SvgDocument svgDoc = SvgDocument.Open(input_SVG);
                ////////var bitmapr = svgDoc.Draw();
                ////////bitmapr.Save(output_SVG, ImageFormat.Png);
                ///////////////////////////////////// try { svgDoc.Draw().Save(output_SVG, ImageFormat.Png); } catch(Exception errr) { log("Could not convert: " + SVG_File.Name + " " + errr); }
                svg2png_draw(output_SVG, input_SVG, SVG_File.Name, 100);
                //Lets see if it is linked, if so, we need to create a page for it or something
                //Main.xml --> conn -->face_view-->face_view.xml-->data
                getLinkedSVG(SVG_Path);
            }
            catch (Exception errr)
            {
                log("Could not convert: " + SVG_Path + " " + errr);
            }
        }

        private void getLinkedSVG(string SVG_Path)
        {
            FileInfo SVG_File = new FileInfo(SVG_Path);
            try
            {
                string xml_name = SVG_File.FullName.Replace(".svg", ".xml");
                string png_name = SVG_File.Name.Replace(".svg", ".png");
                if (File.Exists(xml_name))
                {
                    //We have a linked SVG file

                    //We need to parse the file and remove '&'
                    string text = File.ReadAllText(xml_name);
                    text = text.Replace("&", "AMPERSANDERS");

                    //Parse the descriptor, create dir, set vars and continue to next loop
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(text);


                    XmlNodeList nodes = doc.DocumentElement.SelectNodes("/evtm/page");
                    string title = "";
                    foreach (XmlNode node in nodes)
                    {
                        title = node.SelectSingleNode("title").InnerText;
                        evtm_index.Add(GetSafeFilename(title));
                    }
                    //log(title);

                    log("We have Linked SVG: " + SVG_File.Name + " --> " + title);
                    //string html_page = "<html><head></head><body><p>" + title + "</p><image src=\"" + png_name + "\"></image><table border=\"1\">";                        
                    string html_page = "";
                    curr_path = SVG_File.DirectoryName;
                    if (!File.Exists(SVG_File.DirectoryName + "\\" + GetSafeFilename(title) + ".htm"))
                    {
                        html_page += "<html><head></head><body><p>" + title + "</p><title>" + title + "</title>";
                        html_page += "<script>window.onload = function () {iFrameResize({maxHeight:800,maxWidth:830,minWidth:830,scrolling:true});}; function ReverseDisplay(d) {if(document.getElementById(d).style.display == \"none\") { document.getElementById(d).style.display = \"block\"; }else { document.getElementById(d).style.display = \"none\"; }}</script>";
                        // html_page += "<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js\"></script><script type=\"text/javascript\" src=\"iframeResizer.min.js\"></script>";
                        html_page += "<script src=\"jquery.min.js\"></script><script type=\"text/javascript\" src=\"iframeResizer.min.js\"></script>";
                    }
                    html_page += "<image src=\"" + png_name + "\"></image><table border=\"1\">";

                    nodes = doc.DocumentElement.SelectNodes("/evtm/page/connector_collection/conn");
                    if (nodes.Count >= 1)
                    {
                        html_page += "<th>Connector #</th><th>Location</th><th>Location Diagram</th><th>Pinout</th>";
                    }

                    foreach (XmlNode node in nodes)
                    {
                        html_page += "<tr>";
                        string conn_name = "";
                        string loc_name = "";
                        string loc_view = ""; //--> This is linked to another svg/png/xml --> We care about the XML file for this... In fact we NEED it for the pinout... (Create another html page for it, we shall)
                        string face_view = "";//--> This is linked to an xml which has the svg/png file and data attributes
                        try
                        {
                            conn_name = node.SelectSingleNode("name").InnerText;
                            loc_name = node.SelectSingleNode("loc").InnerText;
                            loc_view = node.SelectSingleNode("loc_view").InnerText;
                            face_view = node.SelectSingleNode("face_view").InnerText;
                            img_num++;
                        }
                        catch { }
                        html_page += "<td>" + conn_name + "</td>";
                        html_page += "<td>" + loc_name + "</td>";
                        html_page += "<td><a onClick=\"ReverseDisplay('img" + img_num + "'); return true; \" href=\"Javascript: return true; \">Show/Hide</a>&nbsp;&nbsp;&nbsp;<a href=\"" + loc_view + ".png\" target=\"_blank\">" + loc_view + "</a><image id=\"img" + img_num + "\"  style=\"display: none;\" src=\"" + loc_view + ".png\"</image></td>"; //Link to the connector location png
                                                                                                                                                                                                                                                                                                                                                         //html_page += "<td><a href=\"" + getConnectorView(face_view) + "\">Connector</a></td>"; // Connector View
                                                                                                                                                                                                                                                                                                                                                         //html_page += "<td><a href=\"" + getConnectorView(face_view) + "\">Connector</a></td>"; // Connector View
                        img_num++;
                        html_page += "<td><a onClick=\"ReverseDisplay('img" + img_num + "'); return true; \" href=\"Javascript: return true; \">Show/Hide</a>&nbsp;&nbsp;&nbsp;<a href=\"" + getConnectorView(face_view) + "\" target=\"_blank\">" + getConnectorView(face_view).Replace(".htm", "") + "</a><iframe id=\"img" + img_num + "\" class=\"iframe\" frameborder=\"0\" id=\"iframe\" style=\"display: none;\" src=\"" + getConnectorView(face_view) + "\">Connector</iframe></td>"; // Connector View
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              //  log(conn_name + " " + loc_name + " " + loc_view + " " + face_view);
                        html_page += "</tr>";

                    }
                    if (nodes.Count >= 1)
                    {
                        html_page += "</table>";
                    }
                    /*splice table setup below
                     * 
                     */

                    nodes = doc.DocumentElement.SelectNodes("/evtm/page/splice_collection/splice");
                    if (nodes.Count >= 1)
                    {
                        html_page += "<table border=\"1\"><th>Splice #</th><th>Location</th><th>Zone</th><th>Location Diagram</th>";
                    }

                    foreach (XmlNode node in nodes)
                    {
                        html_page += "<tr>";
                        string conn_name = "";
                        string loc_name = "";
                        string loc_view = ""; //--> This is linked to another svg/png/xml --> We care about the XML file for this... In fact we NEED it for the pinout... (Create another html page for it, we shall)
                        string zone = "";
                        try
                        {
                            conn_name = node.SelectSingleNode("name").InnerText;
                            loc_name = node.SelectSingleNode("loc").InnerText;
                            loc_view = node.SelectSingleNode("loc_view").InnerText;
                            zone = node.SelectSingleNode("zone").InnerText;
                            img_num++;
                        }
                        catch { }
                        html_page += "<td>" + conn_name + "</td>";
                        html_page += "<td>" + loc_name + "</td>";
                        html_page += "<td>" + zone + "</td>";
                        html_page += "<td><a onClick=\"ReverseDisplay('img" + img_num + "'); return true; \" href=\"Javascript: return true; \">Show/Hide</a>&nbsp;&nbsp;&nbsp;<a href=\"" + loc_view + ".png\" target=\"_blank\">" + loc_view + "</a><image id=\"img" + img_num + "\"  style=\"display: none;\" src=\"" + loc_view + ".png\"</image></td>"; //Link to the connector location png
                        html_page += "</tr>";

                    }
                    if (nodes.Count >= 1)
                    {
                        html_page += "</table>";
                    }
                    /*splice table setup above
                     * 
                     */
                    /*Ground table setup below
               * 
               */

                    nodes = doc.DocumentElement.SelectNodes("/evtm/page/ground_collection/ground");
                    if (nodes.Count >= 1)
                    {
                        html_page += "<table border=\"1\"><th>Ground #</th><th>Location</th><th>Zone</th><th>Location Diagram</th>";
                    }

                    foreach (XmlNode node in nodes)
                    {
                        html_page += "<tr>";
                        string conn_name = "";
                        string loc_name = "";
                        string loc_view = ""; //--> This is linked to another svg/png/xml --> We care about the XML file for this... In fact we NEED it for the pinout... (Create another html page for it, we shall)
                        string zone = "";
                        try
                        {
                            conn_name = node.SelectSingleNode("name").InnerText;
                            loc_name = node.SelectSingleNode("loc").InnerText;
                            loc_view = node.SelectSingleNode("loc_view").InnerText;
                            zone = node.SelectSingleNode("zone").InnerText;
                            img_num++;
                        }
                        catch { }
                        html_page += "<td>" + conn_name + "</td>";
                        html_page += "<td>" + loc_name + "</td>";
                        html_page += "<td>" + zone + "</td>";
                        html_page += "<td><a onClick=\"ReverseDisplay('img" + img_num + "'); return true; \" href=\"Javascript: return true; \">Show/Hide</a>&nbsp;&nbsp;&nbsp;<a href=\"" + loc_view + ".png\" target=\"_blank\">" + loc_view + "</a><image id=\"img" + img_num + "\"  style=\"display: none;\" src=\"" + loc_view + ".png\"</image></td>"; //Link to the connector location png
                        html_page += "</tr>";

                    }
                    if (nodes.Count >= 1)
                    {
                        html_page += "</table>";
                    }
                    /*Ground table setup above
                     * 
                     */

                    /*Fuse table setup below
           * 
           */

                    nodes = doc.DocumentElement.SelectNodes("/evtm/page/fuse_collection/fuse");
                    if (nodes.Count >= 1)
                    {
                        html_page += "<table border=\"1\"><th>Fuse #</th>";
                    }

                    foreach (XmlNode node in nodes)
                    {
                        html_page += "<tr>";
                        string conn_name = "";
                        //string face_page = "";
                        string loc_view = ""; //--> This is linked to another svg/png/xml --> We care about the XML file for this... In fact we NEED it for the pinout... (Create another html page for it, we shall)
                                              // string bv_page = "";
                        try
                        {
                            conn_name = node.SelectSingleNode("name").InnerText;
                            loc_view = node.SelectSingleNode("loc_view").InnerText; //The FUCK is the point of the location view? It doesn't show jack shit about the fuse!
                                                                                    //face_page = node.SelectSingleNode("face_page").InnerText;
                                                                                    // bv_page = node.SelectSingleNode("bestview_page").InnerText;
                            img_num++;
                        }
                        catch { }
                        html_page += "<td>" + conn_name + "</td>";
                        //html_page += "<td><a onClick=\"ReverseDisplay('img" + img_num + "'); return true; \" href=\"Javascript: return true; \">Show/Hide</a>&nbsp;&nbsp;&nbsp;<a href=\"" + loc_view + "\" target=\"_blank\">" + loc_view + ".png</a><image id=\"img" + img_num + "\"  style=\"display: none;\" src=\"" + loc_view + ".png\"</image></td>"; //Link to the connector location png
                        html_page += "</tr>";


                    }
                    if (nodes.Count >= 1)
                    {
                        html_page += "</table>";
                    }
                    /*fuse table setup above
                     * 
                     */


                    //html_page += "</body></html>";
                    //File.WriteAllText(SVG_File.DirectoryName + "\\" + GetSafeFilename(title) + ".htm", html_page);
                    ////File.AppendAllText(SVG_File.DirectoryName + "\\" + GetSafeFilename(title) + ".htm", html_page);
                    Directory.CreateDirectory(SVG_File.DirectoryName + "\\Resources\\Wiring_Diagrams\\");
                    File.AppendAllText(SVG_File.DirectoryName + "\\Resources\\Wiring_Diagrams\\" + GetSafeFilename(title) + ".htm", html_page);

                }
            }
            catch (Exception errr) { log(errr.ToString()); }

        }

        private void createConnectorPage(string path, string title)
        {

            //Create a mimic of the leftside index page
            //Save as Connector_Index.htm
            //Make sure the link in createEVTMIndex references the leftside iframe!!!!!!
            //Also make sure to have a "back" button to get back to the original index page (Wiring_Diagram_Index.htm)
            //path = path.Replace("\\Resources\\Wiring_Diagrams\\", "") + "\\Resources\\Wiring_Diagrams\\"; 
            string[] files = System.IO.Directory.GetFiles(path, "*allcon.xml");
            //string xml_name = path + "\\eerallcon.xml";//Pull data from errallcon.xml (so search for *allcon.xml)
            if (files.Count() > 0)
            {
                string xml_name = files[0];//Pull data from errallcon.xml (so search for *allcon.xml)


                //We need to parse the file and remove '&'
                string text = File.ReadAllText(xml_name);
                text = text.Replace("&", "AMPERSANDERS");

                string html_content = "<HTML><HEAD><META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><style type=\"text/css\">BODY.GRAY {margin-top:1em;margin-right:1em;margin-left:1em;font-family:arial;background-color:E0E0E0;}P.NOSPACE {margin-top:.4em;margin-bottom:.4em;font-size:9pt;font-family:arial;}B.SMALL {font-size:9pt;font-family:arial;}CENTER.SMALL {font-size:9pt;font-family:arial;}DIV.INDENT {font-size:10pt;margin-left:1.5em;}A.SMALL {color:blue;font-size:9pt;font-family:arial;}</style></HEAD><body class=\"gray\"><hr color=\"black\"><p class=\"nospace\"><center class=\"small\">";
                html_content += "<b class=\"small\">" + title + "<br>Wiring Diagrams</b></center><hr color=\"black\">";

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(text);
                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/search/values/conn");
                //Do looping through here
                //Add stuff below
                foreach (XmlNode node in nodes)
                {

                    string conn_name = "";
                    string desc = "";
                    string face_view = "";
                    try
                    {
                        conn_name = node.SelectSingleNode("name").InnerText;
                        desc = node.SelectSingleNode("desc").InnerText;
                        face_view = node.SelectSingleNode("face_view").InnerText;
                    }
                    catch { }
                    html_content += "<br><p class=\"nospace\">";
                    html_content += "<a class=\"small\" href=\"" + face_view + ".htm\" target=\"rightside\">" + conn_name + " -- " + desc + "</a>";

                }

                //Add stuff above

                html_content += "</html>";
                File.WriteAllText(path + "\\Resources\\Wiring_Diagrams\\Connector_Index.htm", html_content);
            }
            else
            {
                log("Wiring Diagram does not contain expected files. This could be because it's an older DVD release.");
            }
        }

        private void createEVTMIndex(string path, string title)
        {

            evtm_index = evtm_index.Distinct().ToList();
            evtm_index.Sort();
            evtm_index = evtm_index.OrderBy(evtm_index => evtm_index).ToList();
            string html_content = "<HTML><HEAD><META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><style type=\"text/css\">BODY.GRAY {margin-top:1em;margin-right:1em;margin-left:1em;font-family:arial;background-color:E0E0E0;}P.NOSPACE {margin-top:.4em;margin-bottom:.4em;font-size:9pt;font-family:arial;}B.SMALL {font-size:9pt;font-family:arial;}CENTER.SMALL {font-size:9pt;font-family:arial;}DIV.INDENT {font-size:10pt;margin-left:1.5em;}A.SMALL {color:blue;font-size:9pt;font-family:arial;}</style></HEAD><body class=\"gray\"><hr color=\"black\"><p class=\"nospace\"><center class=\"small\">";
            html_content += "<b class=\"small\">" + title + "<br>Wiring Diagrams</b></center><hr color=\"black\">";
            foreach (string index in evtm_index)
            {
                //Add <a> to index page
                html_content += "<br><p class=\"nospace\">";
                html_content += "<a class=\"small\" href=\"" + index + ".htm\" target=\"rightside\">" + index + "</a>";

            }
            html_content += "<br><p class=\"nospace\"><br>";
            html_content += "<a class=\"small\" href=\"Connector_Index.htm\" target=\"leftside\">Connector Index</a>";
            html_content += "</html>";
            // 
            log("Path: " + path);
            // path = path.Replace("\\Resources\\Wiring_Diagrams\\", "") + "\\Resources\\Wiring_Diagrams\\";
            // log("Path2: " + path);
            //File.WriteAllText(path + "\\Resources\\Wiring_Diagrams\\Wiring_Diagram_Index.htm", html_content);
            File.WriteAllText(Path.Combine(path , "Wiring_Diagram_Index.htm"), html_content);

            //Create "Main" iframed page to mimic service manual
            string html_main = "<html><frameset cols=\"15%,*\"><frame src=\"Resources\\Wiring_Diagrams\\Wiring_Diagram_Index.htm\" name=\"leftside\"><frame name=\"rightside\" src=\"\" ></frame></frameset></frameset>";

            File.WriteAllText(Path.Combine(path , "Wiring Diagrams.htm"), html_main);
            path = path.Replace("\\Resources\\Wiring_Diagrams", "");
            path = Path.Combine(path, "Resources","Wiring_Diagrams");
            log("Path 2: " + path);            
            createConnectorPage(path, title);
        }

        private void refineEVTM(string title)
        {
            //Create the Resources --> Wiring_Diagrams directory structure and copy files to it
            Directory.CreateDirectory(curr_path + "\\Resources\\Wiring_Diagrams");
            string[] files = System.IO.Directory.GetFiles(curr_path, "*.svg");
            foreach (var file in files)
            {
                svg2png(file);
            }
            createEVTMIndex(curr_path, title);
        }
        private string getConnectorView(string con_view_path)
        {
            string con_view_png = "";

            try
            {
                string html_page = "<html><head></head><body><title>" + con_view_path + "</title><br>";
                // html_page += "<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js\"></script><script type=\"text/javascript\" src=\"iframeResizer.contentWindow.min.js\"></script>";
                html_page += "<script src=\"jquery.min.js\"></script><script type=\"text/javascript\" src=\"iframeResizer.contentWindow.min.js\"></script>";


                XmlDocument doc = new XmlDocument();
                doc.Load(curr_path + "\\" + con_view_path + ".xml");
                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/Faceviews/Connector/Faces/Face");
                foreach (XmlNode node in nodes)
                {

                    /*var noder = node.SelectSingleNode("Face");
                    if(con_view_png.Length != 0)
                    {
                        waitPrompt("WE FOUND ONE WITH MORE THAN ONE ATTRIBUTE!!!!!");
                    }
                    con_view_png += noder.Attributes["File"].Value.ToString();*/
                    con_view_png += node.Attributes["File"].Value.ToString();
                    string gender = "";
                    try { gender = node.Attributes["Gender"].Value.ToString(); } catch { }
                    //con_view_png = noder.Attributes[2].Value.ToString();
                    html_page += "<image hspace = \"15\" src=\"" + node.Attributes["File"].Value.ToString().Replace(".svg", ".png") + "\">" + gender + "</image>        ";
                }
                html_page += "<table style=\"width: 800px; \" border=\"1\"><th>Pin #</th><th>Circuit #</th><th>Color</th><th>Gauge</th><th>Function</th><th>Qualifier</th><th>Used</th>";
                nodes = doc.DocumentElement.SelectNodes("/Faceviews/Connector/Pins/Pin");
                foreach (XmlNode node in nodes)
                {
                    html_page += "<tr>";
                    html_page += "<td>" + node.Attributes["Cavity"].Value.ToString() + "</td>";
                    html_page += "<td>" + node.Attributes["CircuitNumber"].Value.ToString() + "</td>";
                    html_page += "<td>" + node.Attributes["Color"].Value.ToString() + "</td>";
                    html_page += "<td>" + node.Attributes["Guage"].Value.ToString() + "</td>"; //Yes THEY misspelled Gauge
                    html_page += "<td>" + node.Attributes["Function"].Value.ToString() + "</td>";
                    html_page += "<td>" + node.Attributes["Qualifier"].Value.ToString() + "</td>"; //Not sure what this is, need to do some testing...
                    html_page += "<td>" + node.Attributes["Used"].Value.ToString() + "</td>";
                    html_page += "</tr>";
                }
                html_page += "</table></body></html>";
                File.WriteAllText(curr_path + "\\Resources\\Wiring_Diagrams\\" + con_view_path + ".htm", html_page);
            }
            catch (Exception errr) { log(errr.ToString()); }
            //return con_view_png.Replace(".svg",".htm");
            return (con_view_path + ".htm");
        }

        private async void btnExecute_Click(object sender, EventArgs e)
        {
            try {
                pgbMain.Value = 0;
                int selectedIndex = lsbMainSelectionList.SelectedIndex;
                string type = "";
                switch (selectedIndex)
                {
                    case 0: //List all TSO
                        await GetTSOO("", "", "ALL:LIST");
                        break;
                    case 1: //List FSM
                        await GetTSOO("", "", "SERVICE:LIST");
                        break;
                    case 2: //List Wiring Diagrams
                        await GetTSOO("", "", "EVTM:LIST");
                        break;
                    case 3: //List TSBs
                        await GetTSOO("", "", "TSB:LIST");
                        break;
                    case 4: //List Recalls
                        await GetTSOO("", "", "RECALL:LIST");
                        break;
                    case 5: //Extract All
                        getEPL(extractLocation);
                        break;
                    case 6: //By Year
                        //svg2png(@"E:\ServiceManualApp\SM\2005_F-150_EVTM\Resources\New folder\e52151016.svg");
                        break;
                    case 7: //By Model
                        break;
                    case 8: //By Type
                        switch (lsbType.SelectedIndex)
                        {
                            case 0:
                                type = "SERVICE";
                                break;
                            case 1:
                                type = "EVTM";
                                break;
                            case 2:
                                type = "TSB";
                                break;
                            case 3:
                                type = "RECALL";
                                break;
                            case 4:
                                type = "PCED";
                                break;
                            default:
                                break;
                        }
                        await GetTSOO("", "", type);
                        break;
                    case 9: //With Filter
                        type = "";
                        switch (lsbType.SelectedIndex)
                        {
                            case 0:
                                type = "SERVICE";
                                break;
                            case 1:
                                type = "EVTM";
                                break;
                            case 2:
                                type = "TSB";
                                break;
                            case 3:
                                type = "RECALL";
                                break;
                            case 4:
                                type = "PCED";
                                break;
                            default:
                                break;
                        }
                        await GetTSOO(txtYear.Text, txtModel.Text, type);
                        break;
                    case 10: //Extract selected TSO
                        foreach (ListViewItem selectedItem in lsvTSODisplay.SelectedItems)
                        {
                            //closeTSOInterface();
                            //createTSOInterface();
                            string[] base_string = selectedItem.Text.ToString().Split('~');
                            string year = base_string[0];
                            string model = base_string[1];
                            string tso_type = base_string[2];
                            log("Processing: " + modelYear + " " + modelYear + " " + tso_type);
                            await GetTSOO(year, model, tso_type);
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception errr)
            {
                log(errr.ToString());
            }
        }
        private async Task GetTSOO(string one, string two, string three)
        {
            var task = await Task.Run<int>(() => {
                getTSO(one, two, three);
                return 0;
            });

        }

        private void lsbMainSelectionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtModel.Enabled = false;
            txtYear.Enabled = false;
            lsbType.Enabled = false;
            switch (lsbMainSelectionList.SelectedIndex)
            {
                case 6:
                    txtYear.Enabled = true;
                    break;
                case 7:
                    txtModel.Enabled = true;
                    break;
                case 8:
                    lsbType.Enabled = true;
                    break;
                case 9:
                    txtModel.Enabled = true;
                    txtYear.Enabled = true;
                    lsbType.Enabled = true;
                    break;
            }
            if(lsbMainSelectionList.SelectedIndex >=6 && lsbMainSelectionList.SelectedIndex <= 9)
            {
                lsvTSODisplay.Visible = false;
                pnlFilter.Visible = true;
            }
            else
            {
                lsvTSODisplay.Visible = true;
                pnlFilter.Visible = false;
            }
        }       
    }
}
