﻿namespace FSM_Extractor
{
    partial class FSM_Extract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lsbMainSelectionList = new System.Windows.Forms.ListBox();
            this.grpMainSelectionList = new System.Windows.Forms.GroupBox();
            this.btnExecute = new System.Windows.Forms.Button();
            this.lsvTSODisplay = new System.Windows.Forms.ListView();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.pgbMain = new System.Windows.Forms.ProgressBar();
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.lblType = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.lsbType = new System.Windows.Forms.ListBox();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.grpLog = new System.Windows.Forms.GroupBox();
            this.spltMain = new System.Windows.Forms.SplitContainer();
            this.spltSelectLog = new System.Windows.Forms.SplitContainer();
            this.grpMainSelectionList.SuspendLayout();
            this.pnlFilter.SuspendLayout();
            this.grpLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltMain)).BeginInit();
            this.spltMain.Panel1.SuspendLayout();
            this.spltMain.Panel2.SuspendLayout();
            this.spltMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spltSelectLog)).BeginInit();
            this.spltSelectLog.Panel1.SuspendLayout();
            this.spltSelectLog.Panel2.SuspendLayout();
            this.spltSelectLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // lsbMainSelectionList
            // 
            this.lsbMainSelectionList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsbMainSelectionList.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsbMainSelectionList.FormattingEnabled = true;
            this.lsbMainSelectionList.ItemHeight = 20;
            this.lsbMainSelectionList.Items.AddRange(new object[] {
            "List All TSO\'s Available",
            "List All FSM",
            "List All Wiring Diagrams",
            "List All TSBs",
            "List All Recalls",
            "Extract All TSO\'s Available",
            "Extract TSO\'s by Year",
            "Extract TSO\'s by Model",
            "Extract TSO\'s by Type",
            "Extract TSO\'s with Filter",
            "Extract Selected TSO"});
            this.lsbMainSelectionList.Location = new System.Drawing.Point(3, 16);
            this.lsbMainSelectionList.Name = "lsbMainSelectionList";
            this.lsbMainSelectionList.Size = new System.Drawing.Size(258, 269);
            this.lsbMainSelectionList.TabIndex = 0;
            this.lsbMainSelectionList.SelectedIndexChanged += new System.EventHandler(this.lsbMainSelectionList_SelectedIndexChanged);
            // 
            // grpMainSelectionList
            // 
            this.grpMainSelectionList.Controls.Add(this.btnExecute);
            this.grpMainSelectionList.Controls.Add(this.lsbMainSelectionList);
            this.grpMainSelectionList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpMainSelectionList.Location = new System.Drawing.Point(0, 0);
            this.grpMainSelectionList.Name = "grpMainSelectionList";
            this.grpMainSelectionList.Size = new System.Drawing.Size(264, 288);
            this.grpMainSelectionList.TabIndex = 1;
            this.grpMainSelectionList.TabStop = false;
            // 
            // btnExecute
            // 
            this.btnExecute.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnExecute.Location = new System.Drawing.Point(3, 262);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(258, 23);
            this.btnExecute.TabIndex = 1;
            this.btnExecute.Text = "Execute";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // lsvTSODisplay
            // 
            this.lsvTSODisplay.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lsvTSODisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsvTSODisplay.GridLines = true;
            this.lsvTSODisplay.HideSelection = false;
            this.lsvTSODisplay.Location = new System.Drawing.Point(0, 0);
            this.lsvTSODisplay.Name = "lsvTSODisplay";
            this.lsvTSODisplay.Size = new System.Drawing.Size(360, 533);
            this.lsvTSODisplay.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lsvTSODisplay.TabIndex = 2;
            this.lsvTSODisplay.TileSize = new System.Drawing.Size(168, 160);
            this.lsvTSODisplay.UseCompatibleStateImageBehavior = false;
            this.lsvTSODisplay.View = System.Windows.Forms.View.List;
            // 
            // txtOutput
            // 
            this.txtOutput.AcceptsReturn = true;
            this.txtOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOutput.Location = new System.Drawing.Point(3, 16);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtOutput.Size = new System.Drawing.Size(258, 197);
            this.txtOutput.TabIndex = 3;
            this.txtOutput.WordWrap = false;
            // 
            // pgbMain
            // 
            this.pgbMain.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pgbMain.Location = new System.Drawing.Point(3, 213);
            this.pgbMain.Name = "pgbMain";
            this.pgbMain.Size = new System.Drawing.Size(258, 25);
            this.pgbMain.Step = 1;
            this.pgbMain.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbMain.TabIndex = 4;
            // 
            // pnlFilter
            // 
            this.pnlFilter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlFilter.Controls.Add(this.lblType);
            this.pnlFilter.Controls.Add(this.lblModel);
            this.pnlFilter.Controls.Add(this.lblYear);
            this.pnlFilter.Controls.Add(this.lsbType);
            this.pnlFilter.Controls.Add(this.txtModel);
            this.pnlFilter.Controls.Add(this.txtYear);
            this.pnlFilter.Location = new System.Drawing.Point(30, 122);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(296, 159);
            this.pnlFilter.TabIndex = 5;
            this.pnlFilter.Visible = false;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(20, 71);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 13);
            this.lblType.TabIndex = 5;
            this.lblType.Text = "Type:";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(16, 47);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(39, 13);
            this.lblModel.TabIndex = 4;
            this.lblModel.Text = "Model:";
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(23, 20);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(32, 13);
            this.lblYear.TabIndex = 3;
            this.lblYear.Text = "Year:";
            // 
            // lsbType
            // 
            this.lsbType.Enabled = false;
            this.lsbType.FormattingEnabled = true;
            this.lsbType.Items.AddRange(new object[] {
            "FSM",
            "Wiring Diagram",
            "TSB",
            "Recall",
            "PCED"});
            this.lsbType.Location = new System.Drawing.Point(61, 71);
            this.lsbType.Name = "lsbType";
            this.lsbType.Size = new System.Drawing.Size(120, 69);
            this.lsbType.TabIndex = 2;
            // 
            // txtModel
            // 
            this.txtModel.Enabled = false;
            this.txtModel.Location = new System.Drawing.Point(61, 44);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(120, 20);
            this.txtModel.TabIndex = 1;
            // 
            // txtYear
            // 
            this.txtYear.Enabled = false;
            this.txtYear.Location = new System.Drawing.Point(61, 17);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(120, 20);
            this.txtYear.TabIndex = 0;
            // 
            // grpLog
            // 
            this.grpLog.Controls.Add(this.txtOutput);
            this.grpLog.Controls.Add(this.pgbMain);
            this.grpLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpLog.Location = new System.Drawing.Point(0, 0);
            this.grpLog.Name = "grpLog";
            this.grpLog.Size = new System.Drawing.Size(264, 241);
            this.grpLog.TabIndex = 6;
            this.grpLog.TabStop = false;
            // 
            // spltMain
            // 
            this.spltMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.spltMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spltMain.Location = new System.Drawing.Point(0, 0);
            this.spltMain.Name = "spltMain";
            // 
            // spltMain.Panel1
            // 
            this.spltMain.Panel1.Controls.Add(this.spltSelectLog);
            // 
            // spltMain.Panel2
            // 
            this.spltMain.Panel2.Controls.Add(this.lsvTSODisplay);
            this.spltMain.Panel2.Controls.Add(this.pnlFilter);
            this.spltMain.Size = new System.Drawing.Size(636, 537);
            this.spltMain.SplitterDistance = 268;
            this.spltMain.TabIndex = 7;
            // 
            // spltSelectLog
            // 
            this.spltSelectLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltSelectLog.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spltSelectLog.IsSplitterFixed = true;
            this.spltSelectLog.Location = new System.Drawing.Point(0, 0);
            this.spltSelectLog.Name = "spltSelectLog";
            this.spltSelectLog.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltSelectLog.Panel1
            // 
            this.spltSelectLog.Panel1.Controls.Add(this.grpMainSelectionList);
            // 
            // spltSelectLog.Panel2
            // 
            this.spltSelectLog.Panel2.Controls.Add(this.grpLog);
            this.spltSelectLog.Size = new System.Drawing.Size(264, 533);
            this.spltSelectLog.SplitterDistance = 288;
            this.spltSelectLog.TabIndex = 7;
            // 
            // FSM_Extract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 537);
            this.Controls.Add(this.spltMain);
            this.Name = "FSM_Extract";
            this.Text = "FSM Extract";
            this.Load += new System.EventHandler(this.FSM_Extract_Load);
            this.grpMainSelectionList.ResumeLayout(false);
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            this.grpLog.ResumeLayout(false);
            this.grpLog.PerformLayout();
            this.spltMain.Panel1.ResumeLayout(false);
            this.spltMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltMain)).EndInit();
            this.spltMain.ResumeLayout(false);
            this.spltSelectLog.Panel1.ResumeLayout(false);
            this.spltSelectLog.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spltSelectLog)).EndInit();
            this.spltSelectLog.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lsbMainSelectionList;
        private System.Windows.Forms.GroupBox grpMainSelectionList;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.ListView lsvTSODisplay;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.ProgressBar pgbMain;
        private System.Windows.Forms.Panel pnlFilter;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.ListBox lsbType;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.TextBox txtYear;
        private System.Windows.Forms.GroupBox grpLog;
        private System.Windows.Forms.SplitContainer spltMain;
        private System.Windows.Forms.SplitContainer spltSelectLog;
    }
}